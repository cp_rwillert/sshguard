FROM debian:buster

RUN apt-get update && \
    apt-get -yq upgrade && \
    apt-get -yq install git ca-certificates autoconf automake byacc flex gcc python-docutils build-essential
WORKDIR /build
ENTRYPOINT ["/build/build.sh"]
VOLUME ["/build"]
